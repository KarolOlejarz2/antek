package com.karololejarz.antek.controller;

import com.karololejarz.antek.DataMode;
import com.karololejarz.antek.DataOption;
import com.karololejarz.antek.dto.EDSPRequestKeyDataDto;
import com.karololejarz.antek.dto.KibanaEntryDto;
import com.karololejarz.antek.service.EDSPAnalyticsService;
import com.karololejarz.antek.service.EDSPRequestService;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

@Controller
public class UploadController {

  EDSPRequestService edspRequestService;
  EDSPAnalyticsService edspAnalyticsService;

  @Autowired
  public UploadController(EDSPRequestService edspRequestService, EDSPAnalyticsService edspAnalyticsService) {
    this.edspRequestService = edspRequestService;
    this.edspAnalyticsService = edspAnalyticsService;
  }

  private static final String STATUS = "status";

  @GetMapping("/")
  public String index(Model model) {
    model.addAttribute("dataOption", new DataOption());
    return "index";
  }

  @PostMapping("/")
  public String uploadCSVFile(@ModelAttribute("dataOption") DataOption dataOption, @RequestParam("file") MultipartFile file, Model model) {

    if (file.isEmpty()) {
      model.addAttribute("message", "Please select a CSV file to upload.");
      model.addAttribute(STATUS, false);
    } else {
      try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

        CsvToBean<KibanaEntryDto> csvToBean = new CsvToBeanBuilder<KibanaEntryDto>(reader)
            .withType(KibanaEntryDto.class)
            .withIgnoreLeadingWhiteSpace(true)
            .build();

        List<KibanaEntryDto> kibanaEntries = csvToBean.parse();
        List<EDSPRequestKeyDataDto> dtos = edspRequestService.parseEDSPRequests(kibanaEntries);
        List<EDSPRequestKeyDataDto> commentedDtos = edspAnalyticsService.filterCommentedDtos(dtos);
        List<String> uniqueCoIds = edspAnalyticsService.getUniqueCoIds(commentedDtos);
        List<String> uniqueDevices = edspAnalyticsService.getUniqueDevices(commentedDtos);
        List<String> imeiSet = edspAnalyticsService.getImeiSet(commentedDtos);
        List<String> uniqueBscsCodes = edspAnalyticsService.getUniqueBscsCodes(commentedDtos);
        List<String> uniqueTariffCodes = edspAnalyticsService.getUniqueTariffCodes(commentedDtos);
        List<EDSPRequestKeyDataDto> dtosToBeShownAndExported= null;

        System.out.println("Selected DataOption: " + dataOption.getDataMode());
        if(dataOption.getDataMode().equals(DataMode.COMMENTED)) {
          model.addAttribute("results", commentedDtos);
          model.addAttribute("resultsSize", commentedDtos.size());
          dtosToBeShownAndExported = commentedDtos;
          System.out.println("Showing commented");
        } else {
          model.addAttribute("results", dtos);
          model.addAttribute("resultsSize", dtos.size());
          dtosToBeShownAndExported = dtos;
          System.out.println("Showing all");
        }

        model.addAttribute(STATUS, true);
        model.addAttribute("coIds", uniqueCoIds);
        model.addAttribute("coIdsNo", uniqueCoIds.size());
        model.addAttribute("devices", uniqueDevices);
        model.addAttribute("devicesNo", uniqueDevices.size());
        model.addAttribute("imeiSet", imeiSet);
        model.addAttribute("imeiSetSize", imeiSet.size());
        model.addAttribute("bscsCodes", uniqueBscsCodes);
        model.addAttribute("bscsCodesNo", uniqueBscsCodes.size());
        model.addAttribute("tariffs", uniqueTariffCodes);

        Writer writer = new FileWriter("result_" + file.getOriginalFilename());
        StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
        beanToCsv.write(dtosToBeShownAndExported);
        writer.close();

      } catch (Exception ex) {
        model.addAttribute("message", "An error occurred while processing the CSV file.");
        model.addAttribute(STATUS, false);
        System.out.println(ex);
      }
    }

    return "index";
  }
}
