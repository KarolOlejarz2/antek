package com.karololejarz.antek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntekApplication {

	public static void main(String[] args) {
		SpringApplication.run(AntekApplication.class, args);
	}

}
