package com.karololejarz.antek.utils;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

@Component
public class XMLComponent {

  public Document parseDocumentFromXmlString(String xmlString) throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD,""); // Compliant java:S2755
    factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA,""); // Compliant java:S2755
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(xmlString));
    Document doc = builder.parse(is);
    doc.getDocumentElement().normalize();
    return doc;
  }

}
