package com.karololejarz.antek.utils;

import com.karololejarz.antek.dto.XMLNodeDto;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

@Component
public class UtilComponent {

  public static String clearNamespaces(String string) {
    return string.replace("ns1:", "")
        .replace("ns2:", "")
        .replace("ns3:", "")
        .replace("ns4:", "");
  }

  public List<XMLNodeDto> convertNodeListIntoXMLNodeDtoList(NodeList nodeList) {
    int nodeListLength = nodeList.getLength();
    List<XMLNodeDto> result = new ArrayList<>();

    for(int i = 0; i < nodeListLength; i++) {
      result.add(new XMLNodeDto(nodeList.item(i)));
    }

    return result;
  }

  public String getTextContentFromNodeWithoutPrintParam(List<XMLNodeDto> nodes, String printParam) {
    String textContent = nodes.stream()
        .filter(n -> n.getTextContent().startsWith(printParam))
        .findFirst()
        .map(XMLNodeDto::getTextContent)
        .orElse("");

    return textContent.replaceAll(printParam, "");
  }

  public String getTextContentOfNodeLastChild(List<XMLNodeDto> nodes, String printParam) {
    XMLNodeDto dto = nodes.stream()
        .filter(n -> n.getTextContent().startsWith(printParam))
        .findFirst()
        .orElse(null);

    Node lastChild = null;
    if(dto != null) {
      lastChild = dto.getChildNodes().item(dto.getChildNodes().getLength() - 1);
    }

    if(lastChild != null) {
      return lastChild.getTextContent();
    }

    return "";
  }
}
