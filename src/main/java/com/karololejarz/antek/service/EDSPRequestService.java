package com.karololejarz.antek.service;

import com.karololejarz.antek.dto.EDSPRequestKeyDataDto;
import com.karololejarz.antek.dto.KibanaEntryDto;
import com.karololejarz.antek.dto.XMLNodeDto;
import com.karololejarz.antek.utils.UtilComponent;
import com.karololejarz.antek.utils.XMLComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EDSPRequestService {

  UtilComponent utilComponent;
  XMLComponent xmlComponent;

  @Autowired
  public EDSPRequestService(UtilComponent utilComponent, XMLComponent xmlComponent) {
    this.utilComponent = utilComponent;
    this.xmlComponent = xmlComponent;
  }

  private final Logger logger = LoggerFactory.getLogger(EDSPRequestService.class);

  public List<EDSPRequestKeyDataDto> parseEDSPRequests(List<KibanaEntryDto> kibanaEntryDtos) {

    List<EDSPRequestKeyDataDto> requests = new ArrayList<>();
    List<String> messages = kibanaEntryDtos.stream().map(KibanaEntryDto::getMessage).collect(Collectors.toList());
    List<String> xmlStrings = messages.stream()
        .map(UtilComponent::clearNamespaces)
        .map(this::retrieveDataXMLFromMessage)
        .collect(Collectors.toList());

    for (String xmlString : xmlStrings) {
      try {
        Document doc = xmlComponent.parseDocumentFromXmlString(xmlString);

        Node data = doc.getFirstChild();
        NodeList printParameters = data.getFirstChild().getChildNodes(); //7-8
        NodeList docSetParameters = data.getLastChild().getChildNodes();

        requests.add(createDto(printParameters, docSetParameters));
      } catch (Exception ex) {
        logger.error(ex.getMessage());
      }
    }

    return requests;
  }

  private String retrieveDataXMLFromMessage(String message) {
    String[] parts = message.split("message: ");
    if(parts.length != 2) {
      logger.warn("!=2 parts: {}", parts.length);
    }
    String xml = parts[1];
    String[] envelope = xml.split("<S:Body>");
    if(envelope.length != 2) {
      logger.warn("!=2 envelope: {}", envelope.length);
    }

    String body = envelope[1];
    String request = body.replace("<S:Body>", "").replace("</S:Body></S:Envelope>", "");
    String[] requestParts = request.split("</eiMessageContext>");
    if(requestParts.length != 2) {
      logger.warn("!=2 requestParts {}", requestParts.length);
    }
    return requestParts[1].replace("</ExecuteDocumentSetPrintSoaBpRequest>", "");
  }

  private EDSPRequestKeyDataDto createDto(NodeList printParameters, NodeList docSetParameters) {
    StringBuilder comment = new StringBuilder();

    Node name = printParameters.item(0);
    Node sessionId = printParameters.item(2);
    Node date = printParameters.item(3);

    Node commonData = printParameters.item(4); //index for length 7-8 ok
    NodeList commonDataParameters = commonData.getFirstChild().getChildNodes();
    List<XMLNodeDto> commonDataParams = utilComponent.convertNodeListIntoXMLNodeDtoList(commonDataParameters);
    String penaltyService = utilComponent.getTextContentFromNodeWithoutPrintParam(commonDataParams, "PRINT_AGR_PENALTY_SERVICE");
    String penaltyDevice = utilComponent.getTextContentFromNodeWithoutPrintParam(commonDataParams, "PRINT_AGR_PENALTY_DEVICE");
    if(penaltyService.isEmpty()) comment.append("Brak PENALTY_SERVICE. ");
    if(penaltyDevice.isEmpty()) comment.append("Brak PENALTY_DEVICE. ");

    Node iterationalData = printParameters.item(5); //index for length 7-8 ok
    NodeList iterationalDataParameters = iterationalData.getFirstChild().getChildNodes();
    List<XMLNodeDto> iterationalDataParams = utilComponent.convertNodeListIntoXMLNodeDtoList(iterationalDataParameters);

    BigDecimal benefit = null;
    BigDecimal penalty = null;

    String agrBenefit = utilComponent.getTextContentOfNodeLastChild(iterationalDataParams, "PRINT_AGR_BENEFIT");
    if(agrBenefit.isEmpty()) {
      comment.append("Brak AGR_BENEFIT. ");
    } else {
      benefit = new BigDecimal(agrBenefit);
    }
    String agrPenalty = utilComponent.getTextContentOfNodeLastChild(iterationalDataParams, "PRINT_AGR_PENALTY");
    if(agrPenalty.isEmpty()) {
      comment.append("Brak AGR_PENALTY. ");
    } else {
      penalty = new BigDecimal(agrPenalty);
    }

    if(benefit != null && penalty != null) {
      if(benefit.compareTo(penalty) != 0) comment.append("Ulga nie równa się karze. ");
    }

    String coId = utilComponent.getTextContentOfNodeLastChild(iterationalDataParams, "PRINT_CO_ID");
    String deviceName = utilComponent.getTextContentOfNodeLastChild(iterationalDataParams, "PRINT_DEVICE_NAME");
    String deviceImei = utilComponent.getTextContentOfNodeLastChild(iterationalDataParams, "PRINT_DEVICE_IMEI");

    Node bscsCode = docSetParameters.item(1);
    Node tariffCode = docSetParameters.item(2);

    return EDSPRequestKeyDataDto.builder()
          .name(name.getTextContent())
          .sessionId(sessionId.getTextContent())
          .date(date.getTextContent().split("\\+")[0])
          .penaltyService(penaltyService)
          .penaltyDevice(penaltyDevice)
          .agrBenefit(benefit)
          .agrPenalty(penalty)
          .coId(coId)
          .deviceName(deviceName)
          .deviceImei(deviceImei)
          .bscsCode(bscsCode.getTextContent())
          .tariffCode(tariffCode.getTextContent())
          .comment(comment.toString())
          .build();
  }
}
