package com.karololejarz.antek.service;

import com.karololejarz.antek.dto.EDSPRequestKeyDataDto;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EDSPAnalyticsService {

  public List<EDSPRequestKeyDataDto> filterCommentedDtos(Collection<EDSPRequestKeyDataDto> dtos) {
    System.out.println("Dtos total: " + dtos.size());
    return dtos.stream().filter(d -> !d.getComment().isEmpty())
        .sorted(Comparator.comparing(EDSPRequestKeyDataDto::getDate).reversed())
        .collect(Collectors.toList());
  }

  public List<String> getUniqueCoIds(Collection<EDSPRequestKeyDataDto> dtos) {
    return dtos.stream()
        .filter(dto -> !dto.getComment().isEmpty())
        .map(EDSPRequestKeyDataDto::getCoId)
        .sorted()
        .distinct()
        .collect(Collectors.toList());
  }

  public List<String> getUniqueDevices(Collection<EDSPRequestKeyDataDto> dtos) {
    return dtos.stream()
        .filter(dto -> !dto.getComment().isEmpty())
        .map(EDSPRequestKeyDataDto::getDeviceName)
        .sorted()
        .distinct()
        .collect(Collectors.toList());
  }

  public List<String> getImeiSet(Collection<EDSPRequestKeyDataDto> dtos) {
    return dtos.stream()
        .filter(dto -> !dto.getComment().isEmpty())
        .map(EDSPRequestKeyDataDto::getDeviceImei)
        .sorted()
        .distinct()
        .collect(Collectors.toList());
  }

  public List<String> getUniqueBscsCodes (Collection<EDSPRequestKeyDataDto> dtos) {
    return dtos.stream()
        .filter(dto -> !dto.getComment().isEmpty())
        .map(EDSPRequestKeyDataDto::getBscsCode)
        .sorted()
        .distinct()
        .collect(Collectors.toList());
  }

  public List<String> getUniqueTariffCodes (Collection<EDSPRequestKeyDataDto> dtos) {
    return dtos.stream()
        .filter(dto -> !dto.getComment().isEmpty())
        .map(EDSPRequestKeyDataDto::getTariffCode)
        .sorted()
        .distinct()
        .collect(Collectors.toList());
  }
}
