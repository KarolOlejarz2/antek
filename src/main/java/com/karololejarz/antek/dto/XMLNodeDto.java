package com.karololejarz.antek.dto;

import lombok.Getter;
import lombok.ToString;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Getter
@ToString
public class XMLNodeDto {
  String nodeName;
  String nodeValue;
  short nodeType;
  String textContent;
  NodeList childNodes;

  public XMLNodeDto(Node node) {
    this.nodeName = node.getNodeName();
    this.nodeValue = node.getNodeValue();
    this.nodeType = node.getNodeType();
    this.textContent = node.getTextContent();
    this.childNodes = node.getChildNodes();
  }
}
