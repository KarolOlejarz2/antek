package com.karololejarz.antek.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class EDSPRequestKeyDataDto {
  String name; //PAKIET_
  String sessionId; //ORDER_
  String date;
  //commonData
  String penaltyService;
  String penaltyDevice;
  //iterationalData
  BigDecimal agrBenefit;
  BigDecimal agrPenalty;
  String coId;
  String deviceName;
  String deviceImei;
  //docSetParameters
  String bscsCode;
  String tariffCode;
  //comment
  String comment;
}
