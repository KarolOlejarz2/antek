package com.karololejarz.antek.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class KibanaEntryDto {

  @CsvBindByName(column = "@timestamp")
  private String timeStamp;
  @CsvBindByName(column = "@version")
  private String version;
  @CsvBindByName(column = "_id")
  private String id;
  @CsvBindByName(column = "_index")
  private String index;
  @CsvBindByName(column = "_score")
  private String score;
  @CsvBindByName(column = "_type")
  private String type;
  @CsvBindByName(column = "agent.ephemeral_id")
  private String agentEphemeralId;
  @CsvBindByName(column = "agent.hostname")
  private String agentHostName;
  @CsvBindByName(column = "agent.name")
  private String agentName;
  @CsvBindByName(column = "agent.type")
  private String agentType;
  @CsvBindByName(column = "agent.version")
  private String agentVersion;
  @CsvBindByName(column = "beat.name")
  private String beatName;
  @CsvBindByName(column = "ecs.version")
  private String ecsVersion;
  @CsvBindByName(column = "fields.environment")
  private String fieldsEnvironment;
  @CsvBindByName(column = "fields.mo")
  private String fieldsMo;
  @CsvBindByName(column = "fields.site")
  private String fieldsSite;
  @CsvBindByName(column = "host.name")
  private String hostName;
  @CsvBindByName(column = "input.type")
  private String inputType;
  @CsvBindByName(column = "log.file.path")
  private String logFilePath;
  @CsvBindByName(column = "log.level")
  private String logLevel;
  @CsvBindByName(column = "log.offset")
  private String logOffset;
  @CsvBindByName
  private String message;
  @CsvBindByName
  private String tags;
  @CsvBindByName(column = "writetimestamp")
  private String writeTimeStamp;

}
